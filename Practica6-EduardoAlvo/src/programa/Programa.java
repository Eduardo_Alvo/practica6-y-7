package programa;

import clases.Armeria;

public class Programa {

	public static void main(String[] args) {
		Armeria weps = new Armeria();
		
		System.out.println("DAMOS DE ALTA TANTO ARMAS COMO MUNICIONES");
		weps.altaArticulo("K98", 100, "rifle", 1300, "nuevo");
		weps.altaArticulo("Mosin Nagant", 20, "rifle", 1000, "usado");
		weps.altaArticulo("Remington", 40, "escopeta", 50, "nuevo");
		
		weps.altaArticulo("22 LR x50", 100, 50, 0.22, false);
		weps.altaArticulo("360mm x250", 30, 250, 3.6, false);
		weps.altaArticulo("762mm x150", 75, 150, 7.6, true);
		System.out.println();
		
		System.out.println("AHORA DAMOS DE ALTA ALGUNOS CLIENTES");
		weps.altaCliente("12345678A", "Juan Luis");
		weps.altaCliente("23456789B", "Pilar");
		weps.altaCliente("34567890C", "Pepe");
		System.out.println();
		
		System.out.println("AHORA LISTAMOS LAS ARMAS");
		weps.listarArmas();
		System.out.println();
		
		System.out.println("AHORA LAS MUNICIONES");
		weps.listarMuniciones();
		System.out.println();
		
		System.out.println("Y POR ULTIMO LOS CLIENTES");
		weps.listarClientes();
		System.out.println();
		
		System.out.println("AHORA JUAN LUIS SE VA A COMPRAR UN ARTICULO DE CADA");
		weps.crearCarrito("12345678A");
		weps.anadirArticulosCarrito(1, "K98");
		weps.anadirArticulosCarrito(1, "Mosin Nagant");
		weps.anadirArticulosCarrito(1, "Remington");
		weps.anadirArticulosCarrito(1, "22 LR x50");
		weps.anadirArticulosCarrito(1, "360mm x250");
		weps.anadirArticulosCarrito(1, "762mm x150");
		System.out.println();
		
		weps.mostrarCarritos();
		
		System.out.println("A JUAN LUIS LE PARECE BIEN SU CARRITO ASI QUE LO COMPRA");
		weps.compraCarrito("12345678A");
		System.out.println();
		
		System.out.println("COMO PODEMOS COMPROBAR, EL STOCK HA BAJADO EN 1 Y JUAN LUIS HA CONSEGUIDO PUNTOS");
		weps.listarArticulos();
		weps.buscarCliente("12345678A");
		System.out.println();
		
		System.out.println("POR ULTIMO, ELIMINAMOS UN ARMA, UNA MUNICION Y UN CLIENTE");
		weps.bajaArticulo("Remington");
		weps.bajaArticulo("762mm x150");
		weps.bajaCliente("34567890C");
		System.out.println();
		
		System.out.println("Y LISTAMOS LOS QUE QUEDAN");
		weps.listarArticulos();
		weps.listarClientes();
		


	}

}
