package clases;

public class Armas extends Articulos{
	
	private static final int PRECIO_BASE = 300;
	
	private String tipo;
	private int alcance;
	private String estado;
	
	
	public Armas(String nombre, int stock, String tipo, int alcance, String estado) {
		super(nombre, stock);
		this.tipo = tipo;
		this.alcance = alcance;
		this.estado = estado;
	}
	
	
	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public int getAlcance() {
		return alcance;
	}


	public void setAlcance(int alcance) {
		this.alcance = alcance;
	}


	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}

	
	@Override
	public double calcularPrecio() {
		double precio = PRECIO_BASE;
		if (getTipo().equalsIgnoreCase("rifle")) {
			precio *= 2.5;
		} else if (getTipo().equalsIgnoreCase("escopeta")) {
			precio *= 2;
		}
		if (getEstado().equalsIgnoreCase("nuevo")) {
			precio *= 1.25;
		} else {
			precio *= 0.75;
		}
		
		return precio;
	}


	@Override
	public String toString() {
		return "Armas [tipo=" + tipo + ", alcance=" + alcance + ", estado=" + estado + ", toString()="
				+ super.toString() + "]";
	}
	
	

}
