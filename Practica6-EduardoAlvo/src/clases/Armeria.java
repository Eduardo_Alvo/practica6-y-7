package clases;


import java.util.ArrayList;
import java.util.Iterator;

public class Armeria {
	
	private ArrayList<Articulos> listaArticulos;
	private ArrayList<Cliente> listaClientes;
	private ArrayList<Carrito> listaCarritos;
	public Armeria() {
		this.listaArticulos = new ArrayList<Articulos>();
		this.listaClientes = new ArrayList<Cliente>();
		this.listaCarritos = new ArrayList<Carrito>();
	}
	
	public void altaArticulo(String nombre, int stock, String tipo, int alcance, String estado) {
		listaArticulos.add(new Armas(nombre,stock,tipo,alcance,estado));
	}
	
	public void altaArticulo(String nombre, int stock, int cartuchos, double calibre, boolean puntaHueca) {
		listaArticulos.add(new Municiones(nombre,stock,cartuchos,calibre,puntaHueca));
	}
	
	public void altaCliente(String dni, String nombre) {
		listaClientes.add(new Cliente(dni,nombre));
	}
	
	public void crearCarrito(String dni) {
		listaCarritos.add(new Carrito(listaCarritos.size()+1, devuelveCliente(dni)));
	}
	
	public void bajaArticulo(String nombre) {
		Iterator<Articulos> iteradorArticulos =listaArticulos.iterator();
		while (iteradorArticulos.hasNext()) {
			Articulos articulo = iteradorArticulos.next();
			if (articulo.getNombre().equals(nombre)) {
				iteradorArticulos.remove();
			}
		}
		
	}
	
	public void bajaCliente(String dni) {
		Iterator<Cliente> iteradorClientes =listaClientes.iterator();
		while (iteradorClientes.hasNext()) {
			Cliente cliente = iteradorClientes.next();
			if (cliente.getDni().equals(dni)) {
				iteradorClientes.remove();
			}
		}
		
	}
	
	public void eliminarCarrito(int idCarrito) {
		Iterator<Carrito> iteradorCarritos =listaCarritos.iterator();
		while (iteradorCarritos.hasNext()) {
			Carrito carrito = iteradorCarritos.next();
			if (carrito.getIdCarrito() == idCarrito) {
				iteradorCarritos.remove();
			}
		}
		
	}
	
	public boolean existeCliente(String dni) {
		for (Cliente clientes : listaClientes) {
			if(clientes.getDni().equals(dni)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean existeArticulo(String nombre) {
		for (Articulos articulos : listaArticulos) {
			if(articulos.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean existeCarrito(int idCarrito) {
		for (Carrito carrito : listaCarritos) {
			if(carrito.getIdCarrito() == idCarrito) {
				return true;
			}
		}
		return false;
	}
	
	
	public Cliente devuelveCliente(String dni) {
		if (existeCliente(dni)) {
			for (Cliente clientes : listaClientes) {
				if (clientes.getDni().equals(dni)) {
					return clientes;
				}
			}
		}
		return null;
	}
	
	public Articulos devuelveArticulo(String nombre) {
		if (existeArticulo(nombre)) {
			for (Articulos articulos : listaArticulos) {
				if (articulos.getNombre().equals(nombre)) {
					return articulos;
				}
			}
		}
		return null;
	}
	
	
	public boolean articuloExisteEnLista(ArrayList<Articulos> listaArticulosCarrito, String nombre) {
		if (existeArticulo(nombre)) {
			for (Articulos articulos : listaArticulosCarrito) {
				if (articulos.getNombre().equals(nombre)) {
					return false;
				}
			}
		}
		return true;
	}
	
	public ArrayList<Articulos> devuelveListaArticulos(int idCarrito) {
			for (Carrito carritos : listaCarritos) {
				if (carritos.getIdCarrito()==idCarrito) {
					return carritos.getListaArticulos();
				}
			}
		return null;
	}
	
	public void anadirArticulosCarrito(int idCarrito, String nombre) {
		if(existeArticulo(nombre)) {
			if (articuloExisteEnLista(devuelveListaArticulos(idCarrito), nombre)) {
				devuelveListaArticulos(idCarrito).add(devuelveArticulo(nombre));
			}
		}
	}
	
	public void mostrarCarritoCliente(String dni) {
		for (Carrito carritos : listaCarritos) {
			if (carritos.getCliente().getDni().equals(dni)) {
				System.out.println(carritos);
				System.out.print(" ,Precio total " + carritos.calculoPrecioCarrito());
				System.out.println();
			}
		}
	}
	
	public void listarCarritos() {
		for (Carrito carritos : listaCarritos) {
			System.out.println(carritos.getCliente());
			System.out.println(carritos);
		}
	}
	
	public void mostrarCarritos() {
		for (Carrito carritos : listaCarritos) {
			System.out.println(carritos);
		}
	}
	
	public void listarClientes() {
		for (Cliente clientes : listaClientes) {
			System.out.println(clientes);
		}
	}
	
	public void listarArticulos() {
		for (Articulos articulos : listaArticulos) {
			System.out.println(articulos);
		}
	}
	
	public void listarArmas() {
		for(Articulos articulos : listaArticulos) {
			if(articulos instanceof Armas) {
				System.out.println(articulos);
			}
		}
	}
	
	public void listarMuniciones() {
		for(Articulos articulos : listaArticulos) {
			if(articulos instanceof Municiones) {
				System.out.println(articulos);
			}
		}
	}
	
	public void buscarCliente(String dni) {
		System.out.println(devuelveCliente(dni));
	}
	
	public void buscarArticulo(String nombre) {
		System.out.println(devuelveArticulo(nombre));
	}
	
	public void buscarCarrito(int idCarrito) {
		int contador = 0;
		for(Carrito carrito : listaCarritos) {
			if (carrito.getIdCarrito() == idCarrito) {
				System.out.println(carrito);
				contador++;
			}
		}
		
		if (contador < 1) {
			System.out.println("El carrito que buscas no existe");
		}
	}
	
	//Metodos extra
	
	
	public void compraCarrito(String dni) {
		for(Carrito carrito : listaCarritos) {
			if(carrito != null && carrito.getCliente().getDni().equals(dni)) {
				mostrarCarritoCliente(dni);
				for(Articulos articulos : devuelveListaArticulos(carrito.getIdCarrito())) {
					articulos.setStock(articulos.getStock()-1);
				}
				
				System.out.println();
				System.out.println("Sus puntos de cliente son: " + carrito.getCliente().getPuntosCliente());
				System.out.println();
				System.out.println("La cuenta despues del descuento de cliente asciende a: " + (carrito.calculoPrecioCarrito()-carrito.getCliente().getPuntosCliente()));
				System.out.println();
				double puntos = carrito.calculoPrecioCarrito()/10;
				System.out.println("Usted ha ganado " + (int)puntos + " puntos de cliente");
				
				carrito.getCliente().setPuntosCliente((int)puntos);
				System.out.println();
				System.out.println("La compra se ha finalizado con exito");
			}
		}
	}
	
	
}
