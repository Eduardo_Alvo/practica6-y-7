package clases;

public class Municiones extends Articulos{
	
	private static final int PRECIO_BASE = 50;
	
	private int cartuchos;
	private double calibre;
	private boolean puntaHueca;
	
	public Municiones(String nombre, int stock, int cartuchos, double calibre, boolean puntaHueca) {
		super(nombre, stock);
		this.cartuchos = cartuchos;
		this.calibre = calibre;
		this.puntaHueca = puntaHueca;
	}

	public int getCartuchos() {
		return cartuchos;
	}

	public void setCartuchos(int cartuchos) {
		this.cartuchos = cartuchos;
	}

	public double getCalibre() {
		return calibre;
	}

	public void setCalibre(double calibre) {
		this.calibre = calibre;
	}

	public boolean isPuntaHueca() {
		return puntaHueca;
	}

	public void setPuntaHueca(boolean puntaHueca) {
		this.puntaHueca = puntaHueca;
	}

	@Override
	public double calcularPrecio() {
		double precio = PRECIO_BASE;
		if (getCartuchos() > 200) {
			precio *= 1.5;
		} else if (getCartuchos() <= 200 && getCartuchos() >= 100) {
			precio *= 1.1;
		} else {
			precio *= 0.9;
		}
		
		if (isPuntaHueca()) {
			precio *= 1.5;
		} else {
			precio *= 0.5;
		}
		
		return precio;
	}

	@Override
	public String toString() {
		return "Municiones [cartuchos=" + cartuchos + ", calibre=" + calibre + ", puntaHueca=" + puntaHueca
				+ ", toString()=" + super.toString() + "]";
	}
	
	
}
