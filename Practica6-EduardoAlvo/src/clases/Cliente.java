package clases;

public class Cliente {
	
	private String dni;
	private String nombre;
	private int puntosCliente;
	
	public Cliente(String dni, String nombre) {
		this.dni = dni;
		this.nombre = nombre;
		this.puntosCliente = 0;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getPuntosCliente() {
		return puntosCliente;
	}

	public void setPuntosCliente(int puntosCliente) {
		this.puntosCliente = puntosCliente;
	}

	@Override
	public String toString() {
		return "Cliente [dni=" + dni + ", nombre=" + nombre + ", puntosCliente=" + puntosCliente + "]";
	}
	
	
	
	
	
}
