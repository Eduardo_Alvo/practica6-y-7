package clases;

import java.time.LocalDate;

public abstract class Articulos {
	
	private String nombre;
	private int stock;
	private LocalDate fecha;
	
	
	
	public Articulos(String nombre, int stock) {
		this.nombre = nombre;
		this.stock = stock;
		this.fecha = LocalDate.now();
	}
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public LocalDate getFecha() {
		return fecha;
	}
	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	
	
	@Override
	public String toString() {
		return "Articulos [nombre=" + nombre + ", stock=" + stock + ", fecha=" + fecha + "]";
	}
	
	
	
	public abstract double calcularPrecio();
	
	
	

	
	

}
