package clases;

import java.util.ArrayList;

public class Carrito {
	private int idCarrito;
	private Cliente cliente;
	private ArrayList<Articulos> listaArticulos;
	public Carrito(int idCarrito, Cliente cliente) {
		this.idCarrito = idCarrito;
		this.cliente = cliente;
		this.listaArticulos = new ArrayList<Articulos>();
	}
	public int getIdCarrito() {
		return idCarrito;
	}
	public void setIdCarrito(int idCarrito) {
		this.idCarrito = idCarrito;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public ArrayList<Articulos> getListaArticulos() {
		return listaArticulos;
	}
	public void setListaArticulos(ArrayList<Articulos> listaArticulos) {
		this.listaArticulos = listaArticulos;
	}
	@Override
	public String toString() {
		return "Carrito [idCarrito=" + idCarrito + ", cliente=" + cliente + ", listaArticulos=" + listaArticulos + "]";
	}
	
	
	public double calculoPrecioCarrito() {
		double precioTotal = 0;
		for (Articulos articulos : listaArticulos) {
			precioTotal += articulos.calcularPrecio();
		}
		return precioTotal;
	}
	
	
}
