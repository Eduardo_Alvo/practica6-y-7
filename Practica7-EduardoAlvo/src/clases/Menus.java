package clases;

import java.util.Scanner;

public class Menus {
	static Scanner input = new Scanner(System.in);
	GestorPeloton peloton = new GestorPeloton();

	// Clase unicamente para construir los menus

	// Menu para las armas
	public void menuArmas() {
		int opcion;

		do {
			System.out.println();
			System.out.println("********************************");
			System.out.println("* 1- Dar de alta arma          *");
			System.out.println("* 2- Visualizar armas          *");
			System.out.println("* 3- Cambiar nombre a arma     *");
			System.out.println("* 4- Categoria de un arma      *");
			System.out.println("* 5- Numero de armas           *");
			System.out.println("* 6- Salir                     *");
			System.out.println("********************************");
			System.out.println();
			opcion = input.nextInt();

			switch (opcion) {
			case 1:
				System.out.println("Introduzca los datos a continuacion");
				System.out.println();
				peloton.altaArma();
				break;
			case 2:
				System.out.println("A continuacion se mostraran las armas");
				System.out.println();
				peloton.visualizarArmas();
				break;
			case 3:
				peloton.modificarArma();
				break;
			case 4:
				peloton.armaCategoria();
				break;
			case 5:
				peloton.numeroArmas();
				break;
			case 6:
				System.out.println("Saliendo al menu principal");
				break;
			default:
				System.out.println("Introduce una opcion valida");
			}

		} while (opcion != 6);
	}
	
	//Menu para los soldados
	public void menuSoldados() {
		int opcion;

		do {
			System.out.println();
			System.out.println("**********************************");
			System.out.println("* 1- Dar de alta soldado         *");
			System.out.println("* 2- Visualizar soldados         *");
			System.out.println("* 3- Buscar soldado              *");
			System.out.println("* 4- Numero de soldados          *");
			System.out.println("* 5- Proporcion soldado/oficial  *");
			System.out.println("* 6- Salir                       *");
			System.out.println("**********************************");
			System.out.println();
			opcion = input.nextInt();

			switch (opcion) {
			case 1:
				System.out.println("Introduzca los datos a continuacion");
				System.out.println();
				peloton.altaSoldado();
				break;
			case 2:
				System.out.println("A continuacion se mostraran los soldados");
				System.out.println();
				peloton.visualizarSoldados();
				break;
			case 3:
				peloton.buscarSoldado();
				break;
			case 4:
				peloton.numeroSoldados();
				break;
			case 5:
				peloton.proporcion();
				break;
			case 6:
				System.out.println("Saliendo al menu principal");
				break;
			default:
				System.out.println("Introduce una opcion valida");
			}

		} while (opcion != 6);
	}
}
