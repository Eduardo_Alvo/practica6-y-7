package clases;

import java.io.Serializable;

public class Arma implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String nombre;
	private String calibre;
	private String categoria;
	
	public Arma(String nombre, String calibre, String categoria) {
		this.nombre = nombre;
		this.calibre = calibre;
		this.categoria = categoria;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCalibre() {
		return calibre;
	}

	public void setCalibre(String calibre) {
		this.calibre = calibre;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	@Override
	public String toString() {
		return nombre + ", " + categoria + " del calibre " + calibre;
	}
	
	
	
	
	
}
