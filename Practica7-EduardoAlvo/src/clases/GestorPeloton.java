package clases;


import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Scanner;

public class GestorPeloton {

	static Scanner input = new Scanner(System.in);

	ArrayList<Arma> listaArmas;
	ArrayList<Soldado> listaSoldados;

	public GestorPeloton() {
		listaArmas = new ArrayList<Arma>();
		listaSoldados = new ArrayList<Soldado>();
	}

	public ArrayList<Arma> getListaArmas() {
		return listaArmas;
	}

	public void setListaArmas(ArrayList<Arma> listaArmas) {
		this.listaArmas = listaArmas;
	}

	public ArrayList<Soldado> getListaSoldados() {
		return listaSoldados;
	}

	public void setListaSoldados(ArrayList<Soldado> listaSoldados) {
		this.listaSoldados = listaSoldados;
	}



	// Creacion y escritura en fichero de las armas
	public void altaArma() {
		BufferedReader lector = new BufferedReader(new InputStreamReader(System.in));
		try {
			RandomAccessFile f = new RandomAccessFile("src/datosArmas.txt", "rw");
			f.seek(f.length());

			System.out.println("Nombre: ");
			String nombre = lector.readLine();
			nombre = formatearNombre(nombre, 20);
			f.writeUTF(nombre);

			System.out.println("Calibre: ");
			String calibre = lector.readLine();
			f.writeUTF(calibre);

			System.out.println("Categoria: ");
			String categoria = lector.readLine();
			f.writeUTF(categoria);
			
			listaArmas.add(new Arma(nombre, calibre, categoria));

			f.close();
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}
	}

	// Visualizacion de las armas
	public void visualizarArmas() {
		try {
			RandomAccessFile f = new RandomAccessFile("src/datosArmas.txt", "rw");
			String nombre = "";
			String calibre = "";
			String categoria = "";

			boolean finArmas = false;
			do {
				try {
					nombre = f.readUTF();
					System.out.println("Nombre: " + nombre);
					calibre = f.readUTF();
					System.out.println("Calibre: " + calibre);
					categoria = f.readUTF();
					System.out.println("Categoria: " + categoria);
					System.out.println();
				} catch (EOFException e) {
					finArmas = true;
					f.close();
				}

			} while (!finArmas);

		} catch (IOException e) {
			System.out.println("Error");
		}
	}

	// Metodo que mas tarde usaremos para modificar el nombre del arma
	private String formatearNombre(String nombre, int lon) {
		if (nombre.length() > lon) {
			return nombre.substring(0, lon);
		} else {
			for (int i = nombre.length(); i < lon; i++) {
				nombre = nombre + " ";
			}
		}
		return nombre;
	}
	
	//Metodo para buscar un arma en nuestro arraylist
	public Arma buscarArma(String nombre) {
		for (Arma arma : listaArmas) {
			if (arma != null && arma.getNombre().equalsIgnoreCase(nombre)) {
				return arma;
			}
		}
		return null;
	}
	
	//Metodo para cambiar de nombre a un arma
	public void modificarArma() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			String oldArma = "Fusil";
			String newArma = "Cerrojo";
			System.out.println("Nombre del arma que deseas modificar");
			oldArma = in.readLine();
			System.out.println("Nuevo nombre del arma");
			newArma = in.readLine();
			RandomAccessFile f = new RandomAccessFile("src/datosArmas.txt", "rw");
			String nombre;
			boolean finArmas = false;
			boolean armaEncontrada = false;
			do {
				try {
					nombre = f.readUTF();
					if (nombre.trim().equalsIgnoreCase(oldArma)) {
						f.seek(f.getFilePointer() - 22);
						newArma = formatearNombre(newArma, 20);
						f.writeUTF(newArma);
						armaEncontrada = true;
					}
				} catch (EOFException e) {
					f.close();
					finArmas = true;
				}
			} while (!finArmas);
			if (armaEncontrada == false) {
				System.out.println("El arma no esta en el fichero");
			} else {
				System.out.println("El arma ha sido modificado");
			}
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	// Accion extra 1, escribes el nombre del arma y devuelve su categoria
	public void armaCategoria() {
		System.out.println("Introduce el arma cuya categoria quieras conocer");
		String nombre1 = "armita";
		nombre1 = input.nextLine();
		nombre1 = formatearNombre(nombre1, 20);
		if (nombre1 != null) {
			try {
				RandomAccessFile f = new RandomAccessFile("src/datosArmas.txt", "rw");
				String nombre;
				@SuppressWarnings("unused")
				String calibre;
				String categoria;
				boolean finArmas = false;
				System.out.println("Categoria del arma");
				do {
					try {
						nombre = f.readUTF();
						calibre = f.readUTF();
						categoria = f.readUTF();
						if (nombre.equalsIgnoreCase(nombre1)) {
							System.out.println(categoria);
						}
					} catch (EOFException e) {
						try {
							f.close();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						finArmas = true;
					} catch (IOException e) {
						e.printStackTrace();
					}

				} while (!finArmas);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

		} else {
			System.out.println("No has escrito nada");
		}

	}

	// Accion extra 2, recibiendo la categoria te devuelve los nombres de las armas
	// que pertenecen a ella (nos servira mas tarde)
	public void categoriaArma(String categoria1) {
		try {
			RandomAccessFile f = new RandomAccessFile("src/datosArmas.txt", "rw");
			String nombre;
			@SuppressWarnings("unused")
			String calibre;
			String categoria;
			boolean finArmas = false;
			do {
				try {
					nombre = f.readUTF();
					calibre = f.readUTF();
					categoria = f.readUTF();
					if (categoria.equalsIgnoreCase(categoria1)) {
						System.out.println(nombre);
					}
				} catch (EOFException e) {
					try {
						f.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					finArmas = true;
				} catch (IOException e) {
					e.printStackTrace();
				}

			} while (!finArmas);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	// Accion extra 3, contar cuantas armas tenemos en el registro
	public void numeroArmas() {
		try {
			RandomAccessFile f = new RandomAccessFile("src/datosArmas.txt", "r");
			int lineas = 0;
			boolean finArmas = false;
			@SuppressWarnings("unused")
			String lineaActual;
			do {
				try {
					lineaActual = f.readUTF();
					lineas++;
				} catch (EOFException e) {
					try {
						f.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					finArmas = true;
				} catch (IOException e) {
					e.printStackTrace();
				}

			} while (!finArmas);
			System.out.println((lineas/3) + " armas");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	//Empezamos con los secuenciales
	
	//Metodo para dar de alta un soldado
	public void altaSoldado() {
		try {
			System.out.println("Nombre del soldado: ");
			String nombre = input.nextLine();
			System.out.println("Rango del soldado: ");
			String rango = input.nextLine();
			System.out.println("Arma preferida (rifle, carabina,..): ");
			String armaPreferida = input.nextLine();
			System.out.println("Armas que el soldado prefiere: ");
			categoriaArma(armaPreferida);
			System.out.println();
			System.out.println("Arma que se le asignara al soldado: ");
			String nombreArma;
			do {
				System.out.println("Ingrese un arma valida: ");
				nombreArma = input.nextLine();
			} while (buscarArma(nombreArma) != null);
			listaSoldados.add(new Soldado(nombre, rango, armaPreferida, buscarArma(nombreArma)));
			

			ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(new File("src/datosSoldados.txt")));
			escritor.writeObject(listaSoldados);
			escritor.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//Metodo para listar soldados
	public void listarSoldados() {
		for (Soldado soldado : listaSoldados) {
			System.out.println(soldado.toString());
			System.out.println();
		}
	}
	
	//Metodo para visualizar soldados recien cargados, usando el metodo anterios
	@SuppressWarnings("unchecked")
	public void visualizarSoldados() {
		try {
			ObjectInputStream lector = new ObjectInputStream(new FileInputStream(new File("src/datosSoldados.txt")));
			listaSoldados = (ArrayList<Soldado>) lector.readObject();
			listarSoldados();
			lector.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	//Metodo para buscar un soldado
	@SuppressWarnings("unchecked")
	public void buscarSoldado() {
		System.out.println("Introduce el nombre del soldado: ");
		String nombre = input.nextLine();
		ArrayList<Soldado> soldados = new ArrayList<Soldado>();

		try {
			ObjectInputStream lector = new ObjectInputStream(new FileInputStream(new File("src/datosSoldados.txt")));
			soldados = (ArrayList<Soldado>) lector.readObject();
			int contador = 0;
			for (Soldado soldado : soldados) {
				if (soldado.getNombre().equalsIgnoreCase(nombre)) {
					System.out.println(soldado.toString());
					contador++;
				}
			}
			if (contador == 0) {
				System.out.println("El soldado no forma parte del peloton");
			}
			lector.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public int contarSoldados() {
		int contador = 0;
		for (int i = 0; i < listaSoldados.size(); i++) {
			contador++;
		}
		return contador;
	}
	
	
	//Accion extra 1, contar cuantos soldados tenemos en el peloton
	@SuppressWarnings("unchecked")
	public void numeroSoldados() {
		try {
			ObjectInputStream lector = new ObjectInputStream(new FileInputStream(new File("src/datosSoldados.txt")));
			listaSoldados = (ArrayList<Soldado>) lector.readObject();
			System.out.println(contarSoldados() + " soldados");
			lector.close();
			
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	//Accion extra 2, muestra la proporcion de soldados de rango "soldado" respecto al total de soldados del peloton
	@SuppressWarnings("unchecked")
	public void proporcion() {
		try {
			ObjectInputStream lector = new ObjectInputStream(new FileInputStream(new File("src/datosSoldados.txt")));
			listaSoldados = (ArrayList<Soldado>) lector.readObject();
			lector.close();
			int contador = 0;
			double porcentaje;
			if (contarSoldados() > 0) {
				for (Soldado soldado : listaSoldados) {
					if (soldado.getRango().equalsIgnoreCase("soldado")) {
						contador++;
					}
				}
				porcentaje = (contador/contarSoldados()) * 100;
				System.out.println("Hay un " + porcentaje + "% de soldados rasos en el peloton");
			} else {
				System.out.println("No hay hombres en el peloton");
			}
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	

}
