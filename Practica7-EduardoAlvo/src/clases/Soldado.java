package clases;

import java.io.Serializable;

public class Soldado implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String nombre;
	private String rango;
	private String armaPreferida;
	private Arma arma;
	public Soldado(String nombre, String rango, String categoria, Arma arma) {
		this.nombre = nombre;
		this.rango = rango;
		this.armaPreferida = categoria;
		this.arma = arma;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getRango() {
		return rango;
	}
	public void setRango(String rango) {
		this.rango = rango;
	}
	public String getCategoria() {
		return armaPreferida;
	}
	public void setCategoria(String categoria) {
		this.armaPreferida = categoria;
	}
	public Arma getArma() {
		return arma;
	}
	public void setArma(Arma arma) {
		this.arma = arma;
	}
	@Override
	public String toString() {
		return "Soldado [nombre=" + nombre + ", rango=" + rango + ", categoria=" + armaPreferida + ", arma=" + arma + "]";
	}
	
	
	
	
	
	
}
