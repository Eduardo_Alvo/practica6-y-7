package programa;

import java.util.Scanner;


import clases.Menus;

public class Programa {
	
	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		Menus menu = new Menus();
		int opcion;
		
		System.out.println("********************************");
		System.out.println("*  'La linea entre el orden y  *");
		System.out.println("*    el desorden reside en la  *");
		System.out.println("*          logistica'          *");
		System.out.println("*          -Sun Tzu            *");
		System.out.println("********************************");
		System.out.println();
		System.out.println();
		System.out.println("********************************");
		System.out.println("*   Con este programa podras   *");
		System.out.println("* gestionar el equimaniento de *");
		System.out.println("*         un peloton           *");
		System.out.println("********************************");
		System.out.println();
		
		do {
			System.out.println();
			System.out.println("********************************");
			System.out.println("* 1- Armas                     *");
			System.out.println("* 2- Soldados                  *");
			System.out.println("* 3- Salir                     *");
			System.out.println("********************************");
			opcion = input.nextInt();
			
			switch (opcion) {
			case 1:
				menu.menuArmas();
				break;
			case 2:
				menu.menuSoldados();
				break;
			case 3:
				System.out.println("********************************");
				System.out.println("*      Cerrando programa       *");
				System.out.println("********************************");
				break;
			case 4:
				System.out.println("********************************");
				System.out.println("* Introduce una opcion valida  *");
				System.out.println("********************************");
			}
			
			
			
		} while (opcion != 3);
	}

}
